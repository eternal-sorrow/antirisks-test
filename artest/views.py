from rest_framework import viewsets, filters, serializers
from artest.models import Product

# used the drf-url-filters app for filtering
from filters.schema import base_query_params_schema
from filters.mixins import FiltersMixin
from voluptuous.validators import Coerce


product_query_schema = base_query_params_schema.extend(
	{
		'search': str,
		'price_gt': Coerce(float),
		'price_lt': Coerce(float)
	}
)

class ProductSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product
		fields = ('id', 'name', 'quantity', 'price')


class ProductViewSet(FiltersMixin, viewsets.ReadOnlyModelViewSet):
	queryset = Product.objects.all()
	serializer_class = ProductSerializer

	filter_backends = (filters.OrderingFilter,)
	filter_mappings = {
		'search': 'name__search',
		'price_gt': 'price__gt',
		'price_lt': 'price__lt'
	}
	filter_validation_schema = product_query_schema

	ordering_fields = ()
