from django.core.management.base import BaseCommand, CommandError
from artest.artest.models import Product

import pandas as pd


class Command(BaseCommand):
	help = "Imports MS Excel .xls or .xlsx file"

	def add_arguments(self, parser):
		parser.add_argument('filename', nargs=1, type=str)

	def handle(self, *args, **options):
		EUR = 69.75
		xl = pd.read_excel(options['filename'][0],
		                   usecols=[1,3,4],
		                   header=None,
		                   names=['name', 'quantity', 'price'])
		# convert values to numeric
		xl[['quantity', 'price']] = xl[['quantity', 'price']].apply(
			pd.to_numeric,
			errors='coerce'
		)
		# delete subheader and service rows
		xl = xl.dropna(subset=['quantity','price'], how='all')

		# iterator of product objects
		def products(xl):
			for index, row in xl.iterrows():
				name = row['name']

				try:
					quantity = int(row['quantity'])
				except ValueError:
					quantity = None

				price = row['price']

				yield Product(name=name, quantity=quantity, price=price)

		Product.objects.bulk_create(products(xl))
