from django.db import models

# Create your models here.

class Product(models.Model):
	name = models.CharField(max_length=512, db_index=True, verbose_name="Номенклатура")
	quantity = models.IntegerField(null=True, verbose_name="Упаковка")
	price = models.FloatField(null=True, db_index=True, verbose_name="Цена")

	class Meta:
		verbose_name = "Товар"
		verbose_name_plural="Товары"
